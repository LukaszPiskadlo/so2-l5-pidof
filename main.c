#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <ctype.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

// zwraca rozmiar tablicy pids
int getPids(int* pids, char* pName)
{
    static const char* PROC_DIR = "/proc";
    static const char* FILE_NAME = "comm";
    static const int PATH_LENGTH = 50;
    static const int NAME_SIZE = 50;

    DIR* dir;
    struct dirent* entry;
    
    dir = opendir(PROC_DIR);
    if (!dir)
    {
        fprintf(stderr, "Nie mozna otworzyc folderu %s!\n", PROC_DIR);
        exit(EXIT_FAILURE);
    }
    
    int i = 0;
    while ((entry = readdir(dir)))
    {
        if (isdigit(*entry->d_name))
        {
            char path[PATH_LENGTH];
            snprintf(path, sizeof(path), "%s/%s/%s", PROC_DIR, entry->d_name, FILE_NAME);
            int file = open(path, O_RDONLY);
            if (file == -1)
            {
                fprintf(stderr, "Nie mozna otworzyc pliku %s!\n", FILE_NAME);
                exit(EXIT_FAILURE);
            }

            char name[NAME_SIZE];
            ssize_t size = read(file, &name, sizeof(name));
            name[size - 1] = '\0'; // pozbywamy sie znaku nowej linii
            if (strcmp(name, pName) == 0)
                pids[i++] = atoi(entry->d_name);

            close(file);
        }
    }

    closedir(dir);

    return i;
}

int main(int argc, char* argv[])
{
    static const char* KILLALL = "./killall";
    static const int MAX_PIDS = 50;

    if (argc < 2)
    {
        fprintf(stderr, "Brak argumentow programu!\n");
        exit(EXIT_FAILURE);
    }

    int pids[MAX_PIDS];
    int size = getPids(pids, argv[1]);
    
    if (strcmp(argv[0], KILLALL) == 0)
    {
        for (int i = 0; i < size; i++)
        {
            kill(pids[i], SIGINT);
        }
    }
    else if (size > 0)
    {
        for (int i = 0; i < size; i++)
        {
            printf("%d ", pids[i]);
        }
        printf("\n");
    }

    return EXIT_SUCCESS;
}
